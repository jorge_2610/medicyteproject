@if(Auth::user()->hasRole('medic'))
<html lang="en">
  <head>
    <title>{{ config('Medicyte', 'Medicyte') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">{{ Auth::user()->name }}</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <a style="color: white;text-decoration: none;" href="{{ route('logout')}}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <i class="fa fa-arrow-left"></i>
                Logout
            </a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link"  href="/schedule/{{ Auth::user()->idProfile }}">
                  <i class="fa fa-clipboard"></i>
                  <span>Agenda</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  href="{{ url('/patients') }}">
                  <i class="fa fa-users"></i>
                  <span>Pacientes</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/settings') }}">
                  <i class="fa fa-wrench"></i>
                  <span>Configuración</span>
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-10 ml-sm-auto col-lg-10 pt-3 px-4">
            @yield('content')
        </main>

      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
@else
  <script>window.location = "/home";</script>
@endif