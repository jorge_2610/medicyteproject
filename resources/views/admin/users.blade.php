@extends('admin.layout')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<div class="text-center">
    	<h3 style="color: #BF2A4A;">Administrar Usuarios</h3>
    </div><br>
    <div class="text-center">
        <div class="text-center">
            <a id="create-new-backup-button" href="{{ url('addUserView') }}" class="btn btn-primary"
               style="margin-bottom:2em;"><i
                    class="fa fa-plus"></i> Agregar Usuario
            </a>
        </div>
        <div class="text-center">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th class="text-center">Acción</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user -> name  }}</td>
                        <td>{{ $user -> email}}</td>
                        <td class="text-center">
                            <a class="btn btn-xs btn-info"
                              href="{{ route('admin.editUserView', ['id' => $user-> id]) }}">
                                <i class="fa fa-pencil"></i> 
                                Editar
                            </a>
                            <a class="btn btn-xs btn-danger" data-button-type="delete"
                               href="{{ route('admin.deleteUser', ['id' => $user-> id]) }}">
                               <i class="fa fa-trash-o"></i>
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert')
@endsection