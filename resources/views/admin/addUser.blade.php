@extends('admin.layout')

@section('title', "Users")

@section('content')
{{-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script> --}}
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{ asset('css/addPatient.css') }}">
</head>
<div class="container-fluid">
	<div class="row">
		<div class="text-center">
			<form class="contact-us pattern-bg" action="addUser" method="POST" onsubmit="return validarContrasenas()">
				{{csrf_field()}}
				<h3 style="text-align: center;margin-bottom: 30px;">Añadir Usuario</h3>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
					  		<input required name="name" id="name" type='text' class="form-control" placeholder="Ingresa el Nombre">
					 	</div>
			       	</div>
							
			       	<div class="col-xs-12 col-sm-6">
				    	<div class="form-group">
					 		<input required type="email" id="email" name="email" class="form-control" placeholder="Ingresa el Email">
					 	</div>
			       	</div>
			       	<div class="col-xs-12 col-sm-6">
				    	<div class="form-group">
					 		<input required id="contrasena1" name="contrasena1" type='password' class="form-control" placeholder="Ingresa Tu Contraseña">
					 	</div>
			       	</div>
			       	<div class="col-xs-12 col-sm-6">
				    	<div class="form-group">
					 		<input id="contrasena2" name="contrasena2" required type='password' class="form-control" placeholder="Confirma tu Contraseña">
					 	</div>
			       	</div>
			       	<div class="col-sm-3"></div>
			       	<div class="col-xs-12 col-sm-6 col-sm-offset-3">
					  	<select id="role" name="role" style="height: 50px;" required class="form-group form-control">
							@foreach ($roles as $role)
								<option value="{{$role-> name}}"
									@if (old('name') == $role->name) 
										selected="selected"
									@endif>
									{{$role -> description}}
								</option>
							@endforeach
					   	</select>
			        </div>
			    </div>
			    <div id="mensajeError" class="alert alert-danger" style="margin-left: 15px; margin-right: 15px; display: none;" >
						<a href="#" class="alert-link">Las Contraseñas no coinciden.</a>
					</div>
			    <div class="text-center">
			    	<button type="submit" class="btn btn-md btn-primary">Añadir Usuario</button>
			    </div>
			</form>
		</div>
	</div>
</div>
<script>
	function validarContrasenas(){
		var contrasena1, contrasena2;
		contrasena1=document.getElementById("contrasena1").value;
		contrasena2=document.getElementById("contrasena2").value;
		if(contrasena1==contrasena2){
			return true;
		}else {
			document.getElementById("mensajeError").style.display="";
		}
		return false;
	}
</script>
@endsection