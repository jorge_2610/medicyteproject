@extends('admin.layout')

@section('title', "Settings")

@section('content')

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{ asset('css/styleSettings.css') }}">
</head>

<div id='settings' ontouchstart>
	<input checked class='nav' name='nav' type='radio'>
	<span class='nav' style="width: 100%;flex-basis: 100%;">Actualizar Cuenta</span>
	<main class='content'>
		<section id='profile'>
			<form action="updateAdmin/{{ Auth::user()->id }}" method="POST" onsubmit="return validarContrasenas()">
				{{ csrf_field() }}
				<ul>
					<fieldset class='material'>
						<br><br>
						<div>
							<label>Rellene los campos a actualizar.</label>
						</div>
					</fieldset>
					<li >
						<fieldset class='material'>
							<div>
								<input required  name="name" type='text' value='{{ Auth::user()->name }}'>
								<label>Nombre</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br><br>
					<li>
						<fieldset class='material'>
							<div>
								<input required name="emalDefault" type='email' value='{{ Auth::user()->email }}'>
								<label>Correo Electronico Actual</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br><br>
					<li>
						<fieldset class='material'>
							<div>
								<input required name="nuevoEmail" type='email'>
								<label>Nuevo Correo Electronico</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br><br>
					<li>
						<fieldset class='material'>
							<div>
								<input required id="contrasena1" name="contrasena1" type='password'>
								<label>Nueva Contraseña</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br><br>
					<li>
						<fieldset class='material'>
							<div>
								<input id="contrasena2" name="contrasena2" required type='password'>
								<label>Confirma Contraseña</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br><br>
					<div class='large padding noli' style="padding-top: 20px; padding-bottom: 20px;">
						<fieldset class='material-button center'>
							<div>
								<input class='save' type='submit' value='Actualizar'>
							</div>
						</fieldset>
					</div>
				</ul>
				<div id="mensajeError" class="alert alert-danger" style="margin-left: 15px;margin-right: 15px;display: none;">
					<a href="#" class="alert-link">Las contraseñas no coinciden.</a>
				</div>
			</form>
		</section>
	</main>
</div>
<script>
	function validarContrasenas(){
		var contrasena1, contrasena2;
		contrasena1=document.getElementById("contrasena1").value;
		contrasena2=document.getElementById("contrasena2").value;
		alert(contrasena1+" "+contrasena2);
		if(contrasena1==contrasena2){
			return true;
		}else {
			document.getElementById("mensajeError").style.display="";
		}
		return false;
	}
</script>
@endsection