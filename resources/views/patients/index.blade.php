@extends('layout')

@section('title', 'Pacientes')

@section('content')
    <link class="rounded-list" href="{{ asset('css/patient-details.css') }}" rel="stylesheet">
    <h1 style="color: #BF2A4A;text-align: center;">Listado de Pacientes</h1>
    <div style="text-align: center;">
        <input type="text" id="myInput" name="search" onkeyup="myFunction()" placeholder="Buscar Paciente" title="Type in a name">
    </div>
    <ul class="rounded-list" id="myUL">
        @forelse ($patients as $patient)
            <li>
                <a href="{{ route('patients.show', ['id' => $patient-> id]) }}">{{ $patient-> name }}, ({{ $patient-> email }})</a>
            </li>
        @empty
            <li>No hay Pacientes Registrados.</li>
        @endforelse                     
    </ul>
    <script>
        function myFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            ul = document.getElementById("myUL");
            li = ul.getElementsByTagName("li");
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";

                }
            }
        }
    </script>
@endsection