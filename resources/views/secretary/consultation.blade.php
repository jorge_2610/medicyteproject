@extends('secretary.layout')

@section('title', "Consulta")

@section('content')
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="{{ asset('css/AgregarConsulta.css') }}">
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<?php date_default_timezone_set ('America/Mexico_City');  ?>
<fieldset>
  <div class="instaFade text-center">
    <a class="btn btn-primary" style="color: white;background-color: #2495CA;" onclick="location.href='/addPatient'">Agregar Paciente</a>
  </div><br>
</fieldset>
<div class="container">  
  <form id="contact" action="/addConsultation" method="POST">
    {{ csrf_field() }}
    <h3 style="color: #BF2A4A;">Agregar consulta</h3>
    <h4>Completa los campos</h4>
    <fieldset>
      <div class="dateTime" id="selectMedico">
        <div>
          <input type="text" id="buscadorMedico" name="buscadorMedico" onkeyup="filtrarDoctor(); javascript:recargar();" placeholder="Medico" title="Type in a Medic">
        </div>
        <select id="medic_cedule" name="medic_cedule">
         @foreach ($medics as $medic)
         <option value="{{$medic-> cedule}}"
           @if (old('cedule') == $medic->cedule) 
           selected="selected"
           @endif>
           {{$medic-> name}}
         </option>
         @endforeach
       </select>
     </div>
     <div class="dateTime" id="selectPatient">
      <div>
        <input type="text" id="buscadorPaciente" name="buscadorPaciente" onkeyup="filtrarPaciente()" placeholder="Paciente" title="Type in a name">
      </div>
      <select id="idPatient" name="idPatient">
       @foreach ($patients as $patient)
       <option value="{{$patient->id}}"> {{ $patient->name }} </option>
       @endforeach
     </select>
   </div>
 </fieldset>
 <fieldset>
  <div class="dateTime">
    <h4>Fecha: </h4><input placeholder="Fecha" name="date" type="date" min="{{ date('Y-m-d') }}" value="{{ date('Y-m-d') }}" tabindex="2" required>
  </div>
  <div class="dateTime">
    <h4>Hora: </h4><input placeholder="Hora" name="hour" type="time" value="{{ date('G:i:s') }}" tabindex="2" required>
  </div>
</fieldset>
<fieldset>
  <input placeholder="Título" type="text" tabindex="3" name="title" required>
</fieldset>
<fieldset>
  <textarea placeholder="Descripción" tabindex="5" name="description" required></textarea>
</fieldset>
<fieldset>
  <button name="submit" type="submit" id="contact-submit" data-submit="...Agregando">Agregar</button>
</fieldset>
</form>
</div>
<script>
  function filtrarDoctor() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("buscadorMedico");
    filter = input.value.toUpperCase();
    ul = document.getElementById("selectMedico");
    li = ul.getElementsByTagName("option");

    for (i = 0; i < li.length; i++) {
      a = li[i];
      if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }

  function filtrarPaciente() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("buscadorPaciente");
    filter = input.value.toUpperCase();
    ul = document.getElementById("selectPatient");
    li = ul.getElementsByTagName("option");

    for (i = 0; i < li.length; i++) {
      a = li[i];
      if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }
</script>
@endsection