@extends('secretary.layout')

@section('title', "Consulta Secretaria")

@section('content')
<?php date_default_timezone_set ('America/Mexico_City');  ?>
<div class="instaFade text-center">
  <a class="btn btn-primary" style="color: white;background-color: #2495CA;" href="{{ route('consultation.consultation') }}">Añadir Consulta</a>
</div><br>
<div class="row">
	<div class="col-lg-6 col-md-6 col-xs-12 text-center">
   <h4> Fecha: </h4><input id="selectorFecha" type="date" value="{{ $dateSelected }}" tabindex="2" onchange="filtroDoble()" required>
 </div>
 <div class="col-lg-6 col-md-6 col-xs-12 text-center">
   <h4> Doctor: </h4>
   <select id="medic_cedule" name="medic_cedule" onchange="filtroDoble()">
     @foreach ($medics as $medic)
     <option value="{{$medic-> cedule}}"
      @if ($idMedicSelected == $medic->cedule) 
      selected="selected"
      @endif>
      {{$medic-> name}}
    </option>
    @endforeach
  </select>
</div>
</div><br>
<div data-spy="scroll" data-target=".scrollspy">

  <div class="">
    <div class="row trip">
      <div class="col-lg-11 col-md-pull-1 col-md-11 col-xs-10">
        <section id="" class="week"><span id="nameMonth" class="week__number">{{ strftime("%B") }}</span>
          <div class="row">
            <div class="col-lg-2">
              <h2 id="numberDay" class="panel__date">{{ strftime("%d") }}</h2>
              <p id="nameDay" class="panel__day">{{ strftime("%A") }}</p>
            </div>

            <div id="contenido" class="col-lg-10 panel">
              @forelse ($dates as $date)                     
              <ul class="{{ $date->date }} {{ $date->idMedic }}">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> {{ date("h:i a", strtotime($date->hour)) }} </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <p> {{ date("d/m/Y", strtotime($date->date)) }} </p>
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: {{ $date->name }} </h4>
                    </div>
                    <form action="/scheduleSecretaryManage/delete" method="POST">
                      {{csrf_field()}}
                      <input style="display: none;" name="fechaCarga" type="date" value="{{ $date->date }}">
                      <input style="display: none;" name="idCarga" type="number" value="{{ $date->id }}">
                      <input style="display: none;" name="ceduleCarga" type="text" value="{{ $date->idMedic }}">
                      <div class="col-lg-4 panel__title--sub ">
                        <input class='btn btn-primary' type='submit' value='Eliminar'>
                      </div>
                    </form>
                    <div class="col-lg-8 panel__title--sub ">
                      <h4> Motivo: {{ $date->title }} </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a class="btn btn-primary" style="color: white;" href="/consultation/edit/{{ $date->id }}">Editar</a>
                    </div>

                  </div>
                </div>
              </ul>
              @empty
              <li>No hay ninguna cita para hoy.</li>
              @endforelse

            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
<div id="mensajeError" class="alert alert-info" style="display: none;">
  <a href="#" class="alert-link">No hay citas para hoy.</a>
</div>
<script>
  function filtroDoble() {
    var contenido, li;
    contenido = document.getElementById("contenido");
    li = contenido.getElementsByTagName("ul");
    for (i = 0; i < li.length; i++) {
      li[i].style.display = "none";
    }
    var monthNames = [
    "Enero", "Febrero", "Marzo",
    "Abril", "Mayo", "Junio", "Julio",
    "Agosto", "Septiembre", "Octubre",
    "Noviembre", "Diciembre"
    ];
    var weekNames = [
    "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"
    ];
    $fecha = document.getElementById("selectorFecha").value; 
    var now = new Date($fecha);
    now.setDate(now.getDate() + 1);
    document.getElementById("nameDay").innerHTML = weekNames[now.getDay()];
    document.getElementById("nameMonth").innerHTML = monthNames[now.getMonth()];
    document.getElementById("numberDay").innerHTML = now.getDate();
    var cedule = document.getElementById("medic_cedule").value;
    list = document.getElementsByClassName($fecha+" "+cedule);
    for(var i=0; i<list.length; i++){
      list[i].style.display = "";
    }
    if(list.length==0){
      document.getElementById("mensajeError").style.display = "";
    }else{
      document.getElementById("mensajeError").style.display = "none";
    }
  }

  filtroDoble();
</script>
<script src="{{ URL::asset('js/jquery-2.1.3.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
@endsection