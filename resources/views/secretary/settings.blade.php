@extends('secretary.layout')

@section('title', "Settings")

@section('content')

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{ asset('css/styleSettings2.css') }}">
</head>
<div id='settings' ontouchstart>
	<input checked class='nav' name='nav' type='radio'>
	<span class='nav'>Perfil</span>
	<input class='nav' name='nav' type='radio'>
	<span class='nav'>Cuenta</span>
	<main class='content'>
		<section id='profile'>
			<form action="/editSecretary/{{ Auth::user()->email}}" method="POST">
				{{ csrf_field() }}
				<br/>
				<ul>
					<br/>
					<br/>
					<br/>
					<li class="large">
						<fieldset class='material'>
							<div>
								<input required  name="name" type='text' value='{{ $user->name}}'>
								<label>Nombre</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<li class="large">
						<fieldset class='material'>
							<div>
								<input required  name="schoolarity" type='text' value="{{ $user->schoolarity }}">
								<label>Escolaridad</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br/>
					<br/>
					<br/>
					<li class="large">
						<fieldset class='material'>
							<div>
								<input required name="phone" type='number' max="9999999999" value="{{ $user->phone }}">
								<label>Telefono</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br/>
					<br/>
					<br/>
					<li class='large'>
						<fieldset class='material'>
							<div>
								<input required name="birthdate" type='date' value="{{ $user->birthdate }}">
								<label>Fecha de Nacimiento</label>
								<hr>
							</div>
						</fieldset>
					</li>

					<fieldset class='material-button center' style="padding-top: 30px;">
						<div>
							<input class='save' type='submit' value='Guardar'>
						</div>
					</fieldset>

				</ul>
			</form>
		</section>


		<section id='profile'>
			<form action="/updateSecretary/{{ Auth::user()->email}}" method="POST" onsubmit="return validarContrasenas()">
				{{ csrf_field() }}
				<ul>
					<fieldset class='material'>
						<br/>
						<div>
							<label>Rellene los campos a actualizar.</label>
						</div>
					</fieldset>
					<li>
						<fieldset class='material'>
							<div>
								<input required name="emalDefault" type='email' value='{{ Auth::user()->email }}'>
								<label>Correo Electronico Actual</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<li>
						<fieldset class='material'>
							<div>
								<input required name="nuevoEmail" type='email'>
								<label>Nuevo Correo Electronico</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<li>
						<fieldset class='material' style="padding-top: 20px;">
							<div>
								<input required id="contrasena1" name="contrasena1" type='password'>
								<label>Nueva Contraseña</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<li>
						<fieldset class='material' style="padding-top: 20px;">
							<div>
								<input  id="contrasena2" name="contrasena2" required type='password'>
								<label>Confirma Contraseña</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<div class='large padding noli' style="padding-top: 20px; padding-bottom: 20px;">
						<fieldset class='material-button center'>
							<div>
								<input class='save' type='submit' value='Enviar Solicitud'>
							</div>
						</fieldset>
					</div>
				</ul>
				<div id="mensajeError" class="alert alert-danger" style="margin-left: 15px; margin-right: 15px; display: none;" >
					<a href="#" class="alert-link">Los correos no coinciden.</a>
				</div>
			</form>
		</section>
	</main>
	</div>
	<script>
		function validarContrasenas(){
			var contrasena1, contrasena2;
			contrasena1=document.getElementById("contrasena1").value;
			contrasena2=document.getElementById("contrasena2").value;
			if(contrasena1==contrasena2){
				return true;
			}else {
				document.getElementById("mensajeError").style.display="";
			}
			return false;
		}
	</script>

@endsection