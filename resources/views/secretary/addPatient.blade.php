@extends('secretary.layout')

@section('title', "Settings")

@section('content')
{{-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script> --}}
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{ asset('css/addPatient.css') }}">
</head>    
<form class="contact-us pattern-bg" action="/createPatient" method="POST">
	{{csrf_field()}}
	<h3 style="text-align: center;margin-bottom: 30px;">Añadir Paciente</h3>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
		  		<input required name="name" type='text' class="form-control" placeholder="Ingresa el Nombre">
		 	</div>
       	</div>
				
       	<div class="col-xs-12 col-sm-6">
	    	<div class="form-group">
		 		<input required type="email" id="email" name="email" class="form-control" placeholder="Ingresa el Email">
		 	</div>
       	</div>
        
        <div class="col-xs-12 col-sm-6">
		  	<div class="form-group">
				<input required type="date" name="birthdate" id="birthdate" class="form-control" placeholder="Your Website">
		   	</div>
         </div>
				
       	<div class="col-xs-12 col-sm-6">
		  	<select id="idMedic" name="idMedic" style="height: 50px;" required class="form-group form-control">
				@foreach ($medics as $medic)
					<option value="{{$medic-> cedule}}"
						@if (old('cedule') == $medic->cedule) 
							selected="selected"
						@endif>
						{{$medic-> name}}
					</option>
				@endforeach
		   	</select>
        </div>
				
       	<div class="col-sm-12">
	    	<div class="textarea-message form-group">
	      		<textarea id="description" name="description" class="textarea-message form-control" placeholder="Ingresa una breve Descripción..." rows="5"></textarea>
		  	</div>
        </div>
    </div>
    <div class="text-center">
    	<button type="submit" class="btn btn-md btn-primary">Crear Paciente</button>
    </div>
</form>
@endsection