@extends('secretary.layout')

@section('title', 'Pacientes')

@section('content')
<link class="rounded-list" href="{{ asset('css/patient-details.css') }}" rel="stylesheet">
<h2 style="color: #BF2A4A;text-align: center;">Listado de Pacientes</h2>
<div class="col-xs-12 text-center">
    <h4> Doctor: </h4>
    <select id="medic_cedule" name="medic_cedule" onchange="filtrarDoctor()">
        @foreach ($medics as $medic)
        <option value="{{ $medic->cedule }}"
            @if (old('cedule') == $medic->cedule) 
            selected="selected"
            @endif>
            {{$medic-> name}}
        </option>
        @endforeach
    </select>
</div><br>
<ul class="rounded-list" id="myUL">
    @forelse ($patients as $patient)
    <li class="{{ $patient->idMedic }} ">
        <a>{{ $patient->name }}, ({{ $patient->email }})</a>
    </li>
    @empty
    <li>No hay Pacientes Registrados.</li>
    @endforelse                     
</ul>
<div id="mensajeError" class="alert alert-info">
  <a href="#" class="alert-link">El doctor actualmente no tiene ningun paciente asignado.</a>
</div>
<script>
    function filtrarDoctor() {
        var cedule;
        var contenido, li;
        contenido = document.getElementById("myUL");
        li = contenido.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
          li[i].style.display = "none";
      }
      cedule = document.getElementById("medic_cedule").value;
      list = document.getElementsByClassName(cedule);
      for(var i=0; i<list.length; i++){
          list[i].style.display = "";
      }
      if(list.length==0){
          document.getElementById("mensajeError").style.display = "";
      }else{
          document.getElementById("mensajeError").style.display = "none";
      }
  }
  filtrarDoctor();
</script>
@endsection