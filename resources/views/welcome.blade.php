<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Medicyte</title>

    <!-- Bootstrap core CSS -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

     <link href="{{ asset('css/login.css') }}" rel="stylesheet">

    </head>

  <body class="text-center">
    <form class="form-signin" method="POST" href="{{ action('UserController@login') }}">
      {{ csrf_field() }}
      <img class="mb-4" src="../../images/logo.png" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Log in</h1>
      <label for="inputEmail" class="sr-only">Correo</label>
      <input type="email" id="Email" name="email" class="form-control" placeholder="Correo" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="Password" name="password" class="form-control" placeholder="Password" required>
      <div class="checkbox mb-3">
        <label>
          <a class="texto" href="">¿Olvidaste tu contraseña?</a>
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
    </form>
  </body>
</html>