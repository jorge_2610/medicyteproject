@if(Auth::user()->hasRole('admin'))
    <script>window.location = "/testAdmin";</script>
@elseif(Auth::user()->hasRole('medic'))
    <script>window.location = "/patients";</script>
@elseif(Auth::user()->hasRole('secretary'))
    <script>window.location = "/scheduleSecretary";</script>
@else
    <script>window.location = "/login";</script>
@endif
