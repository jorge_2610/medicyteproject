@extends('layout')

@section('title', "Settings")

@section('content')

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{ asset('css/styleSettings.css') }}">
</head>

<div id='settings' ontouchstart>
	<input checked class='nav' name='nav' type='radio'>
	<span class='nav'>Perfil</span>
	<input class='nav' name='nav' type='radio'>
	<span class='nav'>Horario</span>
	<input class='nav' name='nav' type='radio'>
	<span class='nav'>Cuenta</span>
	<main class='content'>
		<section id='profile'>
			<form action="/mod/{{ $user->cedule }}" method="POST">
				{{ csrf_field() }}
				<br/>
				<ul>
					{{-- <li class='large padding avatar'>
						<span style="background-image: url({{ asset('images/House.jpg') }});"></span>
						<div>
							<fieldset class='material-button'>
								<div>
									<input name="imagen" type="file" id="imagen" accept="image/png, .jpeg, .jpg, image/gif" />
								</div>
							</fieldset>
						</div>
					</li> --}}
					<br/>
					<br/>
					<br/>
					<li class="large">
						<fieldset class='material'>
							<div>
								<input required  name="name" type='text' value='{{ $user->name}}'>
								<label>Nombre</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<li class="large">
						<fieldset class='material'>
							<div>
								<input required  name="cedule" type='text' value="{{ $user->cedule }}">
								<label>Cedula</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br/>
					<br/>
					<br/>
					{{-- <li>
						<fieldset class='material'>
							<div>
								<input required name="consultinRoom" type='text' value="Medico Cirujano">
								<label>Titulo</label>
								<hr>
							</div>
						</fieldset>
					</li> --}}
					<li class="large">
						<fieldset class='material'>
							<div>
								<input required name="phone" type='number' max="9999999999" value="{{ $user->phone }}">
								<label>Telefono</label>
								<hr>
							</div>
						</fieldset>
					</li>
					<br/>
					<br/>
					<br/>
					<li class='large'>
						<fieldset class='material'>
							<div>
								<input required name="specialty" type='text' value="{{ $user->specialty }}">
								<label>Especializacion</label>
								<hr>
							</div>
						</fieldset>
					</li>

					<fieldset class='material-button center' style="padding-top: 30px;">
						<div>
							<input class='save' type='submit' value='Guardar'>
						</div>
					</fieldset>

				</ul>
			</form>
		</section>

		<section id='account'>
			<form action="/updt/{{ $user->cedule }}" method="POST">
				{{ csrf_field() }}
				<ul>
					<li>
						<br/>
						<fieldset class='material'>
							<div>
								<label>Horario</label>
								<hr>
							</div>
						</fieldset>
						<fieldset class='material'>
							<div>
								<input required type='time' name="tiempoLlegada" value="{{ $user->ArrivalTime }}">
								<input required type='time' name="tiempoSalida" value="{{ $user->EndingTime }}">
							</div>
						</fieldset>
						<br>
						<fieldset class='material'>
							<div>
								<label>Numero de Consultorio</label>
								<hr>
							</div>
						</fieldset>
						<fieldset class='material'>
							<div>
								<input required type='number' name="numberRoom" value="{{ $user->consultinRoom	}}" min="1">
							</div>
						</fieldset>
					</li>
					<li>
						<br/>
						<fieldset class='material'>
							<div>
								<label>Dias</label>
								<hr>
							</div>
						</fieldset>

						<fieldset class='material-checkbox'>
							<div>
								<input name='days[]' type='checkbox' value="L" <?php if((stripos($user->days,'L'))!==false){
									echo "checked";
								} ?> >
								<div class='check'>
									<span>
									</span>
									<label>Lunes</label>
								</div>
							</div>

							<div>
								<input name='days[]' type='checkbox' value="M" <?php if((stripos($user->days,'M'))!==false){
									echo "checked";
								} ?> >
								<div class='check'>
									<span>
									</span>
									<label>Martes</label>
								</div>
							</div>
							<div>
								<input name='days[]' type='checkbox' value="I" <?php if((stripos($user->days,'I'))!==false){
									echo "checked";
								} ?> >
								<div class='check'>
									<span>
									</span>
									<label>Miercoles</label>
								</div>
							</div>
							<div>
								<input name='days[]' type='checkbox' value="J" <?php if((stripos($user->days,'J'))!==false){
									echo "checked";
								} ?> >
								<div class='check'>
									<span>
									</span>
									<label>Jueves</label>
								</div>
							</div>
							<div>
								<input name='days[]' type='checkbox' value="V" <?php if((stripos($user->days,'V'))!==false){
									echo "checked";
								} ?> >
								<div class='check'>
									<span>
									</span>
									<label>Viernes</label>
								</div>
							</div>
							<div>
								<input name='days[]' type='checkbox' value="S" <?php if((stripos($user->days,'S'))!==false){
									echo "checked";
								} ?> >
								<div class='check'>
									<span>
									</span>
									<label>Sabado</label>
								</div>
							</div>
							<div>
								<input name='days[]' type='checkbox' value="D" <?php if((stripos($user->days,'D'))!==false){
									echo "checked";
								} ?> >
								<div class='check'>
									<span>
									</span>
									<label>Domingo</label>
								</div>
							</div>
						</fieldset>
					</ul>
					<div class='large padding noli'>
						<fieldset class='material-button center'>
							<div>
								<input class='save' type='submit' value='Actualizar'>
							</div>
						</fieldset>
					</div>
				</form>
			</section>


			<section id='profile'>
				<form action="/update/{{ Auth::user()->id }}" method="POST" onsubmit="return validarContrasenas()">
					{{ csrf_field() }}
					<ul>
						<fieldset class='material'>
							<br/>
							<div>
								<label>Rellene los campos a actualizar.</label>
							</div>
						</fieldset>
						<li>
							<fieldset class='material'>
								<div>
									<input required name="emalDefault" type='email' value='{{ Auth::user()->email }}'>
									<label>Correo Electronico Actual</label>
									<hr>
								</div>
							</fieldset>
						</li>
						<li>
							<fieldset class='material'>
								<div>
									<input required name="nuevoEmail" type='email'>
									<label>Nuevo Correo Electronico</label>
									<hr>
								</div>
							</fieldset>
						</li>
						<li>
							<fieldset class='material' style="padding-top: 20px;">
								<div>
									<input required id="contrasena1" name="contrasena1" type='password'>
									<label>Nueva Contraseña</label>
									<hr>
								</div>
							</fieldset>
						</li>
						<li>
							<fieldset class='material' style="padding-top: 20px;">
								<div>
									<input  id="contrasena2" name="contrasena2" required type='password'>
									<label>Confirma Contraseña</label>
									<hr>
								</div>
							</fieldset>
						</li>
						<div class='large padding noli' style="padding-top: 20px; padding-bottom: 20px;">
							<fieldset class='material-button center'>
								<div>
									<input class='save' type='submit' value='Enviar Solicitud'>
								</div>
							</fieldset>
						</div>
					</ul>
					<div id="mensajeError" class="alert alert-danger" style="margin-left: 15px; margin-right: 15px; display: none;" >
						<a href="#" class="alert-link">Los correos no coinciden.</a>
					</div>
				</form>
			</section>
		</main>
	</div>
	<script>
		function validarContrasenas(){
			var contrasena1, contrasena2;
			contrasena1=document.getElementById("contrasena1").value;
			contrasena2=document.getElementById("contrasena2").value;
			if(contrasena1==contrasena2){
				return true;
			}else {
				document.getElementById("mensajeError").style.display="";
			}
			return false;
		}
	</script>
	@endsection