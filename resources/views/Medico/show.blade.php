@extends('layout')

@section('title', "Paciente {$patient-> id}")

@section('content')
<link href="{{ asset('css/patient-details.css') }}" rel="stylesheet">
<div class="text-center">
    <a id="create-new-backup-button" href="/editPatient/{{ $patient->id }}" class="btn btn-primary">Editar Expediente</a>
</div>
<div id="top">
    <div id="cv" class="instaFade">
        <div class="mainDetails">

            <div id="name">
                <h1 class="quickFade delayTwo">Expediente</h1>
                <h2 class="quickFade delayThree">{{ $patient->name }}</h2>
            </div>

            <div id="contactDetails" class="quickFade delayFour">
                <ul>
                    <li>Fecha y Hora: {{$patient->created_at}}</a></li>
                    <li>e: <a href="{{ $patient->email }}" target="_blank">{{ $patient->email }}</a></li>
                    <li>m: {{ $patient->phone }}</li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>

        <div id="mainArea" class="quickFade delayFive">
            <section>
                <article>
                    <div class="sectionTitle">
                        <h1>Información General</h1>
                    </div>

                    <div class="sectionContent">
                        <p>{{$patient->description}}.</p>
                    </div>
                </article>
                <div class="clear"></div>
            </section>

            <section>
                <article>
                    <div class="sectionTitle">
                        <h1>Peso</h1>
                        <h1>Estatura</h1>
                    </div>

                    <div class="sectionContent">
                        <p>{{ $patient->weight }} Kgs. <br><br>
                            {{ $patient->height }} mts.</p>
                        </div>
                    </article>
                    <div class="clear"></div>
                </section>

                <section>
                    <div class="sectionTitle">
                        <h1>Citas</h1>
                    </div>

                    <div class="sectionContent">
                        <a href="/medicalRecord/{{ $patient->id }}"> Ver Historial de Citas</a>
                    </div>
                    <div class="clear"></div>
                </section>

                <section>
                    <article>
                        <div class="sectionTitle">
                            <h1>Padecimientos</h1>
                        </div>

                        <div class="sectionContent">
                            <p>{{ $patient->conditions }}</p>
                        </div>
                    </article>
                    <div class="clear"></div>
                </section>


                <section>
                    <div class="sectionTitle">
                        <h1>Alergias</h1>
                    </div>

                    <div class="sectionContent">
                        <p>{{ $patient->allergies }}</p>                    
                    </div>
                    <div class="clear"></div>
                </section>

                <section>
                    <article>
                        <div class="sectionTitle">
                            <h1>Datos del médico Profesional</h1>
                        </div>

                        <div class="sectionContent">
                            <p>Nombre: {{ $medic->name }}<br>
                                Cédula Profesional: {{ $medic->cedule }}<br>
                                General Info: {{ $medic->specialty }} <br>
                            </p>
                        </div>
                    </article>
                    <div class="clear"></div>
                </section>

            </div>
        </div>
    </div>
    @endsection