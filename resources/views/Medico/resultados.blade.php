@extends('layout')

@section('title', "Paciente {$patient-> id}")

@section('content')
<link href="{{ asset('css/patient-details.css') }}" rel="stylesheet">
{{--<div class="text-center">
    <a id="create-new-backup-button" href="/editPatient/{{ $patient->id }}" class="btn btn-primary">Editar Expediente</a>
</div>--}}
<div id="top">
    <div id="cv" class="instaFade">
        <div class="mainDetails">

            <div id="name">
                <h1 class="quickFade delayTwo">Motivo de la cita</h1>
                <h2 class="quickFade delayThree">{{ $date->title }}</h2>
            </div>

            <div id="contactDetails" class="quickFade delayFour">
                <ul>
                    <li>Fecha: {{$date->date}}</a></li>
                    <li>Hora: {{ date("h:i a", strtotime($date->hour)) }}</a></li>
                    <li>e: <a href="mailto:joe@bloggs.com" target="_blank">{{ $patient->email }}</a></li>
                    <li>m: {{ $patient->phone }}</li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>

        <div id="mainArea" class="quickFade delayFive">
            <section>
                <article>
                    <div class="sectionTitle">
                        <h1>Información General de la Cita </h1>
                    </div>

                    <div class="sectionContent">
                        <p>{{ $date->description }}.</p>
                    </div>
                </article>
                <div class="clear"></div>
            </section>

            <section>
                <article>
                    <div class="sectionTitle">
                        <h1>Medico que lo atendio: </h1>
                    </div>

                    <div class="sectionContent">
                        <p>{{ $medicDate->name }} . </p>
                    </div>
                </article>
                <div class="clear"></div>
            </section>

            <section>
                <div class="sectionTitle">
                    <h1>Analisis</h1>
                </div>

                <div class="sectionContent">
                    @if ( $analisis === null)
                    No se expedio ningun Analisis.
                    @else
                    <p> Centro de Analisis:  {{ $analisis->clinicalLaboratory }} </p>
                    <p> Tipo de Analisis:  {{ $analisis->description }} </p>
                    @endif
                </div>
                <div class="clear"></div>
            </section>

            <section>
                <article>
                    <div class="sectionTitle">
                        <h1>Receta asignada: </h1>
                    </div>

                    <div class="sectionContent">
                        @if ( $prescription === null)
                        No se expedio ninguna Receta.
                        @else
                        <p> {{ $prescription->description }} </p>
                        @endif
                    </div>
                </article>
                <div class="clear"></div>
            </section>


            <section>
                <div class="sectionTitle">
                    <h1>Alergias</h1>
                </div>

                <div class="sectionContent">
                    <p>{{ $patient->allergies }}</p>                    
                </div>
                <div class="clear"></div>
            </section>

            <section>
                <article>
                    <div class="sectionTitle">
                        <h1>Datos del Médico Encargado</h1>
                    </div>

                    <div class="sectionContent">
                        <p>Nombre: {{ $medic->name }}<br>
                            Cédula Profesional: {{ $medic->cedule }}<br>
                            General Info: {{ $medic->specialty }} <br>
                        </p>
                    </div>
                </article>
                <div class="clear"></div>
            </section>

        </div>
    </div>
</div>
@endsection