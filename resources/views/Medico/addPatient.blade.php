@extends('layout')

@section('title', "Settings")

@section('content')
{{-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script> --}}
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{ asset('css/addPatient.css') }}">
</head>    
<form class="contact-us pattern-bg" action="/updatePatient" method="POST">
	{{csrf_field()}}
	<h3 style="text-align: center;margin-bottom: 30px;">Añadir Paciente</h3>
	<div class="row">
		
		<div class="col-xs-12 col-sm-12" style="display: none;">
			<div class="form-group">
				<input required name="id" type='number' class="form-control" value="{{ $patient->id }}">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<input required name="name" type='text' class="form-control" placeholder="Ingresa el Nombre" value="{{ $patient->name }}">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<input required type="email" id="email" name="email" class="form-control" placeholder="Ingresa el Email" value="{{ $patient->email }}">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<input required type="date" name="birthdate" id="birthdate" class="form-control" placeholder="Your Birthday" value="{{ $patient->birthdate }}">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<input required type="number" name="weight" id="weight" step="any" class="form-control" placeholder="introduzca su peso" value="{{ $patient->weight }}">
			</div>
		</div>

		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<input required type="number" name="height" step="any" id="height" class="form-control" placeholder="introduzca su altura" value="{{ $patient->height }}">
			</div>
		</div>

		<div class="col-sm-6">
			<div class="textarea-message form-group">
				<input required type="number" name="phone" id="phone" class="form-control" placeholder="introduzca el numero telefonico" value="{{ $patient->phone }}">
			</div>
		</div>

		<div class="col-sm-12">
			<div class="textarea-message form-group">
				<textarea name="condition" class="textarea-message form-control" placeholder="Ingresa los padecimientos que posee el paciente" rows="5">{{ $patient->conditions }}</textarea>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="textarea-message form-group">
				<textarea name="allergies" class="textarea-message form-control" placeholder="Ingrese las alergias que padece el paciente" rows="5">{{ $patient->allergies }}</textarea>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="textarea-message form-group">
				<textarea id="description" name="description" class="textarea-message form-control" placeholder="Ingresa una breve Descripción..." rows="5">{{ $patient->description }}</textarea>
			</div>
		</div>
	</div>
	<div class="text-center">
		<button type="submit" class="btn btn-md btn-primary">Actualizar Ficha Medica</button>
	</div>
</form>
@endsection