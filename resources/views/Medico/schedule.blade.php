@extends('layout')

@section('title', "Agenda Médica")

@section('content')

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="{{ asset('css/styleAgenda.css') }}">
</head>
<?php date_default_timezone_set ('America/Mexico_City');  ?>
<div class="col-lg-11 col-md-pull-1 col-md-11 col-xs-10 text-center">
  <h4> Fecha: </h4><input id="selectorFecha" type="date" value="{{ date('Y-m-d') }}" tabindex="2" onchange="filtrarFecha()" required>
</div>
<div data-spy="scroll" data-target=".scrollspy">

  <div class="">
    <div class="row trip">
      <div class="col-lg-11 col-md-pull-1 col-md-11 col-xs-10">
        <section id="" class="week"><span id="nameMonth" class="week__number">{{ strftime("%B") }}</span>
          <div class="row">
            <div class="col-lg-2">
              <h2 id="numberDay" class="panel__date">{{ strftime("%d") }}</h2>
              <p id="nameDay" class="panel__day">{{ strftime("%A") }}</p>
            </div>

            <div id="contenido" class="col-lg-10 panel">
              @forelse ($dates as $date)                     
              <ul class="{{ $date->date }} {{ $date->active }}">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> {{ date("h:i a", strtotime($date->hour)) }} </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a class="btn btn-primary" style="color: white;" href="/diagnostic/{{ $date->id }}">Diagnosticar</a>
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: {{ $date->name }} </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a class='btn btn-primary' style="color: white;" href="{{ route('patients.show', ['id' => $date->idPatient]) }}" >Ver Detalles</a>
                    </div>
                  </div>
                </div>
              </ul>
              @empty
              <li>No hay ninguna cita para hoy.</li>
              @endforelse

            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
<div id="mensajeError" class="alert alert-info" style="display: none;">
  <a href="#" class="alert-link">No hay citas para hoy.</a>
</div>
<script>
  function filtrarFecha() {
    var contenido, li;
    contenido = document.getElementById("contenido");
    li = contenido.getElementsByTagName("ul");
    for (i = 0; i < li.length; i++) {
      li[i].style.display = "none";
    }
    var monthNames = [
    "Enero", "Febrero", "Marzo",
    "Abril", "Mayo", "Junio", "Julio",
    "Agosto", "Septiembre", "Octubre",
    "Noviembre", "Diciembre"
    ];
    var weekNames = [
    "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"
    ];
    $fecha = document.getElementById("selectorFecha").value; 
    var now = new Date($fecha);
    now.setDate(now.getDate() + 1);
    document.getElementById("nameDay").innerHTML = weekNames[now.getDay()];
    document.getElementById("nameMonth").innerHTML = monthNames[now.getMonth()];
    document.getElementById("numberDay").innerHTML = now.getDate();
    list = document.getElementsByClassName($fecha);
    for(var i=0; i<list.length; i++){
      list[i].style.display = "";
    }
    list2 = document.getElementsByClassName('0');
    for(var i=0; i<list.length; i++){
      list2[i].style.display = "none";
    }
    if(list.length==0){
      document.getElementById("mensajeError").style.display = "";
    }else{
      document.getElementById("mensajeError").style.display = "none";
    }
  }

  filtrarFecha();
</script>
<script src="{{ URL::asset('js/jquery-2.1.3.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
@endsection
