@extends('secretary.layout')

@section('title', "Consulta")

@section('content')
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="{{ asset('css/AgregarConsulta.css') }}">
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  <script>
    $(document).ready(function(){
      $("#r1,#r2,#a1,#a2,#a3").hide();
      $("#receta").click(function(){
        $("#r1").toggle(1000);
        $("#r2").toggle(1000);
      });
      $("#analisis").click(function(){
        $("#a1").toggle(1000);
        $("#a2").toggle(1000);
        $("#a3").toggle(1000);
      });
    });
  </script>
</head>
<?php date_default_timezone_set ('America/Mexico_City');  ?>
<div class="container">  
  <form id="contact" action="" method="post">
    <h3 style="color: #BF2A4A;">Agregar consulta</h3>
    <h4>Completa los campos</h4>
    <fieldset>
      <input placeholder="Paciente" type="text" tabindex="1" disabled="true" value="{{$patient->name}}" autofocus>
    </fieldset>
    <fieldset>
      <div class="dateTime">
        <h4>Fecha: </h4><input placeholder="Fecha" type="date" tabindex="2" required>
      </div>
      <div class="dateTime">
        <h4>Hora: </h4><input placeholder="Hora" type="time" tabindex="2" required>
      </div>
    </fieldset>
    <fieldset>
      <input placeholder="Título" type="text" tabindex="3" required>
    </fieldset>
    <fieldset>
      <textarea placeholder="Descripción" tabindex="5" required></textarea>
    </fieldset>
    <fieldset>
      <h4>Se expidió: </h4>
      <input type="checkbox" id="receta" value="receta"> Receta<br>
      <input type="checkbox" id="analisis" value="analisis"> Análisis
      <p id="r1">Receta</p> 
      <textarea id="r2" placeholder="Descripción" tabindex="5"></textarea>
      <p id="a1">Análisis</p>
      <input id="a2" placeholder="Centro de diagnostico" type="text" tabindex="1">
      <textarea id="a3" placeholder="Descripción" tabindex="5"></textarea>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit" data-submit="...Agregando">Agregar</button>
    </fieldset>
  </form>
</div>
@endsection