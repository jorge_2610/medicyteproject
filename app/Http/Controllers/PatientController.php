<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class PatientController extends Controller
{
    public function index()
    {
        //$patients = DB::table('patient')->get();

        if (Auth::check()){
            $idProfile = Auth::user()->idProfile;
        }
        else {
            $idProfile = '1';
        }

        $patients = Patient::where('idMedic',$idProfile)->get();
        $title = 'Listado de Pacientes';

//        return view('patients.index')
//            ->with('patients', Patient::all())
//            ->with('title', 'Listado de usuarios');

        return view('patients.index', compact('title', 'patients'));
    }

    public function show($id)
    {
        $patient = Patient::find($id);

        return view('patients.show', compact('patient'));
    }
}
