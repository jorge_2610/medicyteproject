ss<?php

namespace App\Http\Controllers;

use App\User;
use App\Patient;
use App\Secretary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
     public function index()
    {
        return view('welcome');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(Request $request, $id){
        $email = $request->input('nuevoEmail');
        $pass = $request->input('contrasena1');
        $user = User::find($id);
        $user->email = $email;
        $user->password = bcrypt($pass);
        $user->save();
        //User::find($id)->update(['active' => 1]);
        return view('home');
    }

    public function editSecretary(Request $request, $emailToGet){
        $email = $request->input('nuevoEmail');
        $pass = $request->input('contrasena1');
        $user = User::where('email', $emailToGet)->first();
        $user->email = $email;
        $user->password = bcrypt($pass);
        $user->save();
        Secretary::where('email', $emailToGet)->update(['email' => $email]);
        return view('home');
    }
}
