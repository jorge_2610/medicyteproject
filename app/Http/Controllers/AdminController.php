<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Artisan;
use Log;
use Storage;
use App\Role;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function test()
    {
        return view('admin.test');
    }

    public function settings()
    {
        return view('admin.settings');
    }

    public function updateAdmin(Request $request, $id){
        $email = $request->input('nuevoEmail');
        $pass = $request->input('contrasena1');
        $name = $request->input('name');
        $user = User::find($id);
        $user->email = $email;
        $user->password = bcrypt($pass);
        $user->name = $name;
        $user->save();
        return view('home');
    }

    public function addUser(Request $request){
        $roleToFind = $request->input('role');
        $role_user = Role::where('name', $roleToFind)->first();
        $password = $request->input('contrasena1');
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($password);
        $user->idProfile = rand(1,99999999);
        $user->save();
        $user->roles()->attach($role_user);
        $users = User::all();
        return redirect()->back();
    }

    public function index()
    {
        $disk = Storage::disk(config('../storage/app/backups'));
        $files = $disk->files(config('../storage/app/backups'));
        $backups = [];
        foreach ($files as $k => $f) {
            // only take the zip files into account
            if (substr($f, -4) == '.sql' && $disk->exists($f)) {
                $backups[] = [
                    'file_path' => $f,
                    'file_name' => str_replace(config('../storage/app/backups') . '/', '', $f),
                    'file_size' => $this->human_filesize($disk->size($f)),
                    'last_modified' => $disk->lastModified($f),
                ];
            }
        }
        // reverse the backups, so the newest one would be on top
        $backups = array_reverse($backups);
        return view("admin.test")->with(compact('backups'));
    }

    public function restore($file_name){
        $dbhost = '127.0.0.1';
        $dbname = 'medicyte';
        $dbuser = 'root';
        $dbpass = 'root';
        $dbdir = '../storage/app/backups/'. $file_name;
        $command = "C:\UniServerZ\core\mysql\bin\mysql --password=root --user=root medicyte < " . $dbdir;
        system($command,$output);
        alert()->success('Éxito.','La recuperación se ha realizado correctamente')->autoclose(3000);
        return redirect()->back();
    }

    public function download($file_name)
    {
        $file = config('../storage/app/backups') . '/' . $file_name;
        $disk = Storage::disk(config('../storage/app/backups'));
        if ($disk->exists($file)) {
            $fs = Storage::disk(config('../storage/app/backups'))->getDriver();
            $stream = $fs->readStream($file);
            return \Response::stream(function () use ($stream) {
                fpassthru($stream);
            }, 200, [
                "Content-Length" => $fs->getSize($file),
                "Content-disposition" => "attachment; filename=\"" . basename($file) . "\"",
            ]);
        } else {
            abort(404, "The backup file doesn't exist.");
        }
    }

    public function delete($file_name)
    {
        $disk = Storage::disk(config('../storage/app/backups'));
        if ($disk->exists(config('../storage/app/backups') . '/' . $file_name)) {
            $disk->delete(config('../storage/app/backups') . '/' . $file_name);
            return redirect()->back();
        } else {
            abort(404, "The backup file doesn't exist.");
        }
    }

    function human_filesize($bytes,$decimals=2){
        if($bytes < 1024){
            return $bytes . ' B';
        }

        $factor = floor(log($bytes,1024));

        return sprintf("%.{$decimals}f ",$bytes / pow(1024,$factor)) . ['B','KB','MB','GB','TB','PB'][$factor];
    }

    public function respaldoPhp(){
        $dbhost = '127.0.0.1';
        $dbname = 'medicyte';
        $dbuser = 'root';
        $dbpass = 'root';
        $backup_file = $dbname . '_' . date("Y-m-d-H-i-s") . '.sql';
        $dbdir = '../storage/app/backups/'. $backup_file;
        $command = "C:\UniServerZ\core\mysql\bin\mysqldump --user=$dbuser --password=$dbpass --host=$dbhost --databases medicyte > $dbdir";
        system($command,$output);
        alert()->success('Éxito.','El Backup se ha realizado correctamente')->autoclose(3000);
        return redirect()->back();
    }

    public function users(){
        $users = User::all();
        return view('admin.users', compact('users'));
    }

    public function addUserView(){
        $roles = Role::all();
        return view('admin.addUser',compact('roles'));
    }

    public function editUserView($id){
        if($id != Auth::user()->id){
            $user = User::find($id);
            $roles = Role::all();
            return view('admin.editUser',compact('user','roles'));
        }
        else {
            return view('admin.settings');
        }
    }

    public function editUser(Request $request){
        $roleToFind = $request->input('role');
        $role_user = Role::where('name', $roleToFind)->first();
        $email = $request->input('email');
        $nuevoEmail = $request->input('nuevoEmail');
        if($nuevoEmail == null){
            $nuevoEmail = $email;
        }
        $pass = $request->input('contrasena1');
        $name = $request->input('name');
        $user = User::where('email',$email)->first();
        DB::table('role_user')->where('user_id', '=', $user->id)->delete();
        $user->email = $nuevoEmail;
        $user->password = bcrypt($pass);
        $user->name = $name;
        $user->save();
        $users = User::all();
        $user->roles()->attach($role_user);
        return view('admin.users', compact('users'));
    }

    public function deleteUser($id){
        if($id != Auth::user()->id){
            DB::table('role_user')->where('user_id', '=', $id)->delete();
            DB::table('users')->where('id', '=', $id)->delete();
            alert()->success('Éxito.','Usuario Eliminado correctamente')->autoclose(3000);
            return redirect()->back();
        }
        else {
            alert()->warning('Advertencia.','No puedes eliminar tu Usuario')->autoclose(3000);
            return redirect()->back();
        }
    }
}

