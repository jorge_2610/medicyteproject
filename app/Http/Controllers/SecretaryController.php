<?php

namespace App\Http\Controllers;
//require_once __DIR__ . '/../vendor/autoload.php';
require_once "../vendor/josemmo/Facturae-PHP/src/Facturae.php";
require_once "../vendor/josemmo/Facturae-PHP/src/FacturaeCentre.php";
require_once "../vendor/josemmo/Facturae-PHP/src/FacturaeItem.php";
require_once "../vendor/josemmo/Facturae-PHP/src/FacturaeParty.php";
require_once '../vendor/dompdf/dompdf/src/Autoloader.php';


use josemmo\Facturae\Facturae;
use josemmo\Facturae\FacturaeParty;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Secretary;
use App\Consultation;
use App\User;
use App\Patient;
use App\Medic;
use PDF;
use Dompdf\Dompdf;


class SecretaryController extends Controller
{
	public function patients()
	{

		$patients = Patient::all();
		$medics = Medic::all();

		return view('secretary.patients', compact('patients','medics'));
	}

	public function schedule(){
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')->orderBy('hour')->get();
		$medics = DB::table('Medics')->get();
		return view('secretary.schedule', compact('dates','medics'));
	}

	public function scheduleManage(){
		$dateSelected = date('Y-m-d');
		$idMedicSelected = Medic::first()->cedule;
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')->orderBy('hour')->get();
		$medics = DB::table('Medics')->get();
		return view('secretary.scheduleManage', compact('dates','medics','idMedicSelected','dateSelected'));
	}

	public function filter($cedule)
	{
		//$dates = Consultation::where('idMedic', $cedule)->get();
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')
		->where('Consultations.idMedic', $cedule)->orderBy('hour')->get();
		$medics = DB::table('Medics')->get();
		return view('secretary.schedule', compact('dates','medics'));
	}

	public function settings(){
		$email = Auth::user()->email;
		$user = Secretary::where('email',$email)->first();
		return view('secretary.settings', compact('user'));
	}

	public function edit(Request $request, $email){
		$name = $request->input('name');
		$user = User::where('email', $email)->first();
		$user->name = $name;
		$user->save();
		Secretary::where('email', $email)->update($request->except(['_token']));
		return view('home');
	}

	public function patientFormView (){
		$medics = Medic::all();
		return view ('secretary.addPatient', compact('medics'));
	}

	public function createPatient(Request $request){
		$patient = new Patient();
		$patient->name = $request->input('name');
		$patient->email = $request->input('email');
		$patient->idMedic = $request->input('idMedic');
		$patient->birthdate = $request->input('birthdate');
		$patient->description = $request->input('description');
		$patient->phone = 12345678;
		$patient->save();
		$patients = Patient::all();
		$medics = Medic::all();
		return view('secretary.patients', compact('patients','medics'));
	}

	public function consultationForm(){
		$patients = Patient::all();
		$medics = Medic::all();	
		return view('secretary.consultation',compact('patients','medics'));
	}

	public function addConsultation(Request $request){
		$consultation = new Consultation();
		$consultation->date = $request->input('date');
		$consultation->hour = $request->input('hour');
		$consultation->idMedic = $request->input('medic_cedule');
		$consultation->title = $request->input('title');
		$consultation->description = $request->input('description');
		$consultation->idPatient = $request->input('idPatient');
		$consultation->costo = 0;
		$consultation->save();
		$patients = Patient::all();
		$medics = Medic::all();	
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')->orderBy('hour')->get();
		$medics = DB::table('Medics')->get();
		return view('secretary.schedule', compact('dates','medics'));
	}

	public function deleteDate(Request $request){
		$id = $request->input('idCarga');
		$idMedicSelected = $request->input('ceduleCarga');
		$dateSelected = $request->input('fechaCarga');
		Consultation::destroy($id);
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')->orderBy('hour')->get();
		$medics = DB::table('Medics')->get();
		return view('secretary.scheduleManage', compact('dates','medics', 'idMedicSelected', 'dateSelected'));	
	}


	public function editDate($id){
		$date = Consultation::find($id);
		$patient = Patient::find($date->idPatient);
		$medics = Medic::all();	
		return view('secretary.EditConsultation',compact('patient','medics', 'date'));	
	}

	public function updateDate(Request $request){
		$id = $request->input('idConsulta');
		$consulta = Consultation::find($id);
		$consulta->date = $request->input('date');
		$consulta->hour = $request->input('hour');
		$consulta->title = $request->input('title');
		$consulta->description = $request->input('description');
		$consulta->idMedic = $request->input('medic_cedule');
		$consulta->save();
		$idMedicSelected = $consulta->idMedic;
		$dateSelected = $consulta->date;
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')->orderBy('hour')->get();
		$medics = DB::table('Medics')->get();
		return view('secretary.scheduleManage', compact('dates','medics', 'idMedicSelected', 'dateSelected'));	
	}
	
}
