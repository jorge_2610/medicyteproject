<?php

namespace App\Http\Controllers;

use App\Medic;
use App\Patient;
use App\User;
use App\Consultation;
use App\Prescription;
use App\Analysis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MedicoController extends Controller{

	public function settings(){
		$idProfile = Auth::user()->idProfile;
		$user = Medic::where('cedule',$idProfile)->get()->first();
		return view('Medico.settings', compact('user'));
	}

	public function edit(Request $request, $id){
		$name = $request->input('name');
		$user = User::where('idProfile', $id)->first();
		$user->name = $name;
		$user->idProfile = $request->input('cedule');
		$user->save();
		Medic::where('cedule', $id)->update($request->except(['_token']));
		$idProfile = Auth::user()->idProfile;
		$user = Medic::where('cedule',$idProfile)->get()->first();
		return view('Medico.settings', compact('user'));
	}

	public function editTime(Request $request, $id){
		$tiempoLlegada = $request->input('tiempoLlegada');
		$tiemposalida = $request->input('tiempoSalida');
		$numeroConsultorio = $request->input('numberRoom');
		$dias = $request->input('days');
		$cadena = "";
		foreach ($dias as $dia => $day) {
			$cadena = $cadena.''.$day;
		}
		Medic::where('cedule', $id)->update(['ArrivalTime' => $tiempoLlegada]);
		Medic::where('cedule', $id)->update(['EndingTime' => $tiemposalida]);
		Medic::where('cedule', $id)->update(['consultinRoom' => $numeroConsultorio]);
		Medic::where('cedule', $id)->update(['days' => $cadena]);
		$idProfile = Auth::user()->idProfile;
		$user = Medic::where('cedule',$idProfile)->get()->first();
		return view('Medico.settings', compact('user'));
	}

	public function schedule($cedule)
	{
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')
		->where('Consultations.idMedic', $cedule)->orderBy('hour')->get();
		return view('Medico.schedule', compact('dates'));
	}

	public function editDiagnostic($id)
	{
		$date = Consultation::find($id);
		$patient = Patient::find($date->idPatient);
		$analisis = Analysis::where('idConsultation', $id)->get()->first();
		$prescription = Prescription::where('idConsultation', $id)->get()->first();
		$medics = Medic::all();
		return view('Medico.diagnostic', compact('patient','medics', 'date', 'analisis', 'prescription'));
	}

	public function updateDiagnostic(Request $request)
	{
		$id = $request->input('id');
		$date = Consultation::find($id);
		$date->costo = $request->input('costo');
		$date->title = $request->input('title');
		$date->description = $request->input('description');
		$date->active = false;
		$date->save();
		
		if($request->input('prescription')!=""){
			$prescription = new Prescription();
			$prescription->idConsultation = $id;
			$prescription->description = $request->input('prescription');
			$prescription->save();
		}

		if($request->input('analisis')!=""){
			$analisis = new Analysis();
			$analisis->idConsultation = $id;
			$analisis->clinicalLaboratory = $request->input('clinicalAnalisis');
			$analisis->description = $request->input('analisis');
			$analisis->save();
		}
		$dates = DB::table('Consultations')
		->join('Patients', 'Patients.id', '=', 'Consultations.idPatient', 'inner', false)
		->select('Consultations.*', 'Patients.name')
		->where('Consultations.idMedic', $date->idMedic)->orderBy('hour')->get();
		return view('Medico.schedule', compact('dates'));
	}

	public function showPatient($id)
	{
		$patient = Patient::find($id);
		$medic = Medic::where('cedule', $patient->idMedic)->get()->first();
		return view('Medico.show', compact('patient','medic'));
	}

	public function editPatient($id)
	{
		$medics = Medic::all();
		$patient = Patient::find($id);
		return view('Medico.addPatient', compact('patient','medics'));
	}

	public function updatePatient(Request $request){
		$id = $request->input('id');
		$patient = Patient::find($id);
		$patient->name = $request->input('name');
		$patient->email = $request->input('email');
		$patient->phone = $request->input('phone');
		$patient->birthdate = $request->input('birthdate');
		$patient->description = $request->input('description');
		$patient->weight = $request->input('weight');
		$patient->height = $request->input('height');
		$patient->conditions = $request->input('condition');
		$patient->allergies = $request->input('allergies');
		$patient->save();
		$medic = Medic::where('cedule', $patient->idMedic)->get()->first();
		return view('Medico.show', compact('patient','medic'));
	}
	public function medicalRecord($id){
//		$dates = DB::table('Consultations2')->where('idPatient', $id);
		$dates = Consultation::where('idPatient', $id)->orderBy('date')->orderBy('hour')->get();
		$patient = Patient::find($id);
		return view('Medico.medicalRecord', compact('dates','patient'));	
	}

	public function medicalResults($id){
		$date = Consultation::find($id);
		$patient = Patient::find($date->idPatient);
		$medic = Medic::where('cedule', $patient->idMedic)->get()->first();
		$medicDate = Medic::where('cedule', $date->idMedic)->get()->first();
		$analisis = Analysis::where('idConsultation', $id)->get()->first();
		$prescription = Prescription::where('idConsultation', $id)->get()->first();
		return view('Medico.resultados', compact('date','patient','medic','medicDate', 'analisis','prescription'));	
	}

}
