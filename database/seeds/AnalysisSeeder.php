<?php

use App\Analysis;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnalysisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Analysis::class)->create([
    		'clinicallaboratory' => "La paz laboratorio de Analisis Clinicos",
    		'description' => "Se realizo un analisis de sangre que dio positivo.",
    		'idConsultation' => 3,
    	]);

    	factory(Analysis::class)->create([
    		'clinicalLaboratory' => "La paz laboratorio de Analisis Clinicos",
    		'description' => "Se realizo un analisis de sangre que dio positivo.",
    		'idConsultation' => 12,
    	]);

    	factory(Analysis::class)->create([
    		'clinicalLaboratory' => "La paz laboratorio de Analisis Clinicos",
    		'description' => "Se realizo un analisis de sangre que dio positivo.",
    		'idConsultation' => 13,
    	]);

    	factory(Analysis::class)->create([
    		'clinicalLaboratory' => "La paz laboratorio de Analisis Clinicos",
    		'description' => "Se realizo un analisis de sangre que dio positivo.",
    		'idConsultation' => 9,
    	]);
    }
}
