<?php

use App\Consultation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsultationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$hoy = strftime( "%Y-%m-%d", time());
      $hora = date("H:i", time());

      factory(Consultation::class)->create([
        'date' => '2018-04-17',
        'hour' => '07:00:00', 
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Revision",
        'description' => "La consulta fue realizada con el fin de realizar una revision de rutina, no presento nada fuera de lo comun, su peso se ha mantenido bien, ha crecido algunos centimetros y posee buena salud.",
        'idPatient' => 1,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-17',
        'hour' => '07:00:00', 
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Revision",
        'description' => "La consulta fue realizada con el fin de realizar una revision de rutina.",
        'idPatient' => 1,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-14',
        'hour' => '07:00:00',
        'costo' => 0,
        'idMedic' => 12345678,
        'title' => "Dolor en el Pecho",
        'description' => "El paciente presentaba un agudo dolor en el pecho, todo resultado de un golpe en esa area se reviso y no presento ningun problema adicional.",
        'idPatient' => 1,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-03-2',
        'hour' => '09:00:00',
        'costo' => 0,
        'idMedic' => 12345678,
        'title' => "Revision por Gripe",
        'description' => "El paciente presentaba signos de gripe comun, y estaba empezando a desarrollar tos seca, sus sintomas empezaron a notarse alrededor de dos dias atras.",
        'idPatient' => 1,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-03-22',
        'hour' => '07:00:00',
        'costo' => 0,
        'idMedic' => 12345678,
        'title' => "Problemas Estomacales",
        'description' => "El paciente presentaba signos de intoxicacion por ingerir alimentos en mal estado de la calle, se mando a hacer analisis de sangre y orina.",
        'idPatient' => 1,
        'active' => false,        
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-01-22',
        'hour' => '07:00:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Problemas Estomacales",
        'description' => "El paciente presentaba signos de intoxicacion por ingerir alimentos en mal estado de la calle, se mando a hacer analisis de sangre y orina.",
        'idPatient' => 1,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-06-14',
        'hour' => '07:00:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Revision de rutina",
        'description' => "Revision mensual de rutina.",
        'idPatient' => 1,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-17',
        'hour' => '09:00:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Problemas Estomacales",
        'description' => "El paciente presentaba un caso de diarrea no infecciosa y algo de fiebre generada por la misma enfermedad.",
        'idPatient' => 3,
      ]);
      
      factory(Consultation::class)->create([
        'date' => '2018-05-17',
        'hour' => '10:30:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Dolor en el timpano",
        'description' => "El paciente presentaba dolor en ambos oidos, se realizo una revision y se encontro que sufria de alguna infeccion ocasionada por el polvo.",
        'idPatient' => 4,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-15',
        'hour' => '12:00:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Dolor de Garganta",
        'description' => "El paciente presentaba un dolor en la garganta severo, generado de un problema en sus vias respiratorias.",
        'idPatient' => 3,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-13',
        'hour' => '14:00:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Dolor de Garganta",
        'description' => "El paciente presentaba un dolor en la garganta severo, generado de un problema en sus vias respiratorias.",
        'idPatient' => 2,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-15',
        'hour' => '15:00:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Revision por Gripe",
        'description' => "El paciente presentaba gripe comun, y algunos sintomas de fiebre. Su peso y salud son normales y solo necesita algunos refuerzos.",
        'idPatient' => 4,
        'active' => false,
      ]);


      factory(Consultation::class)->create([
        'date' => '2018-05-15',
        'hour' => '17:00:00',
        'costo' => 0,
        'idMedic' => 87654321,
        'title' => "Revision por Gripe",
        'description' => "El paciente presentaba gripe comun, y algunos sintomas de fiebre. Su peso y salud son normales y solo necesita algunos refuerzos.",
        'idPatient' => 1,
        'active' => false,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-17',
        'hour' => '15:00:00',
        'costo' => 0,
        'idMedic' => 215544438,
        'title' => "Revision por Gripe",
        'description' => "El paciente presentaba gripe comun, y algunos sintomas de fiebre. Su peso y salud son normales y solo necesita algunos refuerzos.",
        'idPatient' => 10,
      ]);


      factory(Consultation::class)->create([
        'date' => '2018-05-17',
        'hour' => '17:00:00',
        'costo' => 0,
        'idMedic' => 215544438,
        'title' => "Revision por Gripe",
        'description' => "El paciente presentaba gripe comun, y algunos sintomas de fiebre. Su peso y salud son normales y solo necesita algunos refuerzos.",
        'idPatient' => 9,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-17',
        'hour' => '12:00:00',
        'costo' => 0,
        'idMedic' => 12345678,
        'title' => "Dolor de Garganta",
        'description' => "El paciente presentaba un dolor en la garganta severo, generado de un problema en sus vias respiratorias.",
        'idPatient' => 12,
      ]);

      factory(Consultation::class)->create([
        'date' => '2018-05-17',
        'hour' => '14:00:00',
        'costo' => 0,
        'idMedic' => 12345678,
        'title' => "Dolor de cabeza",
        'description' => "El paciente presentaba un dolor en la cabeza severo, generado de un problema en sus sus habitos de descanso.",
        'idPatient' => 15,
      ]);

//        factory(Consultation::class, 10)->create();
    }
  }
