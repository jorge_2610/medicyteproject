<?php

use App\Prescription;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrescriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Prescription::class)->create([
    		'description' => "La receta dada fue: Ampicilina 500 mg cada 6 horas",
    		'idConsultation' => 3,
    	]);

    	factory(Prescription::class)->create([
    		'description' => "La receta dada fue: Dipirona  600 mg cada 8 horas",
    		'idConsultation' => 17,
    	]);

    	factory(Prescription::class)->create([
    		'description' => "La receta dada fue: Nimesulida 500 mg cada 6 horas",
    		'idConsultation' => 5,
    	]);
    	factory(Prescription::class)->create([
    		'description' => "La receta dada fue: Bisolvon 2 cucharadas cada 4 horas",
    		'idConsultation' => 4,
    	]);
	//    	factory(Prescription::class, 10)->create();
    }
}
