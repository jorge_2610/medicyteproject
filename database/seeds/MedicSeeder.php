<?php

use App\Medic;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Medic::class)->create([
            'name' => 'Dorothy Hansine Andersen',
            'cedule' => 12345678,
            'phone' => 3331096534,
            'consultinRoom' => 6,
            'days' => 'LMIJV',
            'ArrivalTime' => '07:00:00',
            'EndingTime' => '15:00:00',
            'specialty' => 'Ginecología',
        ]);

        factory(Medic::class)->create([
            'name' => 'James Wilson',
            'cedule' => 87654321,
            'phone' => 3345432234,
            'consultinRoom' => 12,
            'days' => 'LMIJV',
            'ArrivalTime' => '07:00:00',
            'EndingTime' => '15:00:00',
            'specialty' => 'Medicina Familiar',
        ]);

        factory(Medic::class)->create([
            'name' => 'Gregory House',
            'cedule' => 205544438,
            'phone' => 331235466,
            'consultinRoom' => 22,
            'days' => 'LMIJV',
            'ArrivalTime' => '07:00:00',
            'EndingTime' => '15:00:00',
            'specialty' => 'Enfermedades Infecciosas y Nefrología',
        ]);

        factory(Medic::class)->create([
            'name' => 'Amber Volakis',
            'cedule' => 215544438,
            'phone' => 331235466,
            'consultinRoom' => 3,
            'days' => 'LMIJV',
            'ArrivalTime' => '07:00:00',
            'EndingTime' => '15:00:00',
            'specialty' => 'Enfermedades Infecciosas y Nefrología',
        ]);

        factory(Medic::class, 10)->create();
    }
}
