<?php

use App\Secretary;
use Illuminate\Database\Seeder;

class SecretarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $secretary = new Secretary();
        $secretary->name = 'Jessica adams';
        $secretary->email = 'JessicaAdamns@hotmail.com';
        $secretary->phone = 3316273761;
        $secretary->schoolarity = 'Licenciatura en Administración';
        $secretary->birthdate = '1990-05-01';
        $secretary->sex = 'H';
        $secretary->save();
    }
}
