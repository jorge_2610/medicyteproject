<?php

use App\Patient;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Patient::class)->create([
            'name' => 'Pedro Jimenez',
            'email' => 'PedroJi@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-03-20',
            'height' => "1.80",
            'weight' => "70",
            'description' => 'Persona deportista, frecuentemente se lesiona. Presenta contusiones frecuentemente.',
            'allergies' => 'Ninguna',
            'conditions' => 'Ninguna',
            'idMedic' => '87654321',
        ]);

        factory(Patient::class)->create([
            'name' => 'Jesus Rodriguez Mendez',
            'email' => 'Jesus@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-01-22',
            'height' => "1.80",
            'weight' => "85",
            'description' => 'Chico Enfermizo, asiste frecuentemente bajo casos de gripe y tos.',
            'allergies' => 'Ninguna',
            'conditions' => 'Ninguna',
            'idMedic' => '87654321',
        ]);

        factory(Patient::class)->create([
            'name' => 'Laura Lopez Cardenas',
            'email' => 'Laura@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-06-07',
            'height' => "1.50",
            'weight' => "50",
            'description' => 'Chica de Buena Salud. Asiste comunmente a revisiones de rutina y por certificados medicos.',
            'allergies' => ' Polen y Penicilina',
            'conditions' => 'Presenta algunos casos de Epilepsia.',
            'idMedic' => '87654321',
        ]);

        factory(Patient::class)->create([
            'name' => 'Jose Ramon Jimenez Ortiz',
            'email' => 'JoseRamon@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-12-20',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Adulto sedentario, con vida ocupada y mala alimentacion.',
            'allergies' => 'Ninguna.',
            'conditions' => 'Sobrepeso',
            'idMedic' => '87654321',
        ]);        

        factory(Patient::class)->create([
            'name' => 'Jose Roberto Ortiz',
            'email' => 'Roberto@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-03-21',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Presenta problemas de alcoholismo.',
            'allergies' => 'Polvo.',
            'conditions' => 'Ninguna',
            'idMedic' => '87654321',
        ]);                

        factory(Patient::class)->create([
            'name' => 'Alberto Savedra Rojo',
            'email' => 'Alberto@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-10-28',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Adulto con problemas de alimentacion.',
            'allergies' => 'Comino',
            'conditions' => 'Enfermedad Pulmonar cevera.',
            'idMedic' => '87654321',
        ]);

        factory(Patient::class)->create([
            'name' => 'Camila Cardenas Carrillo',
            'email' => 'Camila@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-10-28',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Chica joven y de buena salud.',
            'allergies' => 'Ninguna.',
            'conditions' => 'Ceguera cronica.',
            'idMedic' => '87654321',
        ]);

        factory(Patient::class)->create([
            'name' => 'David Alberto Torres',
            'email' => 'David@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-10-28',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Chico con Fibrosis pulmonar idiopática, asiste a revisiones mensuales.',
            'allergies' => 'Ninguna.',
            'conditions' => 'Fibrosis pulmonar idiopática',
            'idMedic' => '87654321',
        ]);

        factory(Patient::class)->create([
            'name' => 'Karol Ramirez ',
            'email' => 'Karol@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-03-21',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Chica activa',
            'allergies' => 'Ninguna.',
            'conditions' => 'Ninguna.',
            'idMedic' => '87654321',
        ]);

        factory(Patient::class)->create([
            'name' => 'Fernando Martin Jaime Ramirez',
            'email' => 'Fercho@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-03-21',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Melanoma metastásico',
            'allergies' => 'Ninguna.',
            'conditions' => 'Ninguna.',
            'idMedic' => '215544438',
        ]);

        factory(Patient::class)->create([
            'name' => 'Erick Castañeda',
            'email' => 'Erick@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-03-21',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Listeriosis',
            'allergies' => 'Ninguna.',
            'conditions' => 'Ninguna.',
            'idMedic' => '205544438',
        ]);

        factory(Patient::class)->create([
            'name' => 'Alan Ugalde',
            'email' => 'Alan@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-03-21',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Chico con Mala Alimentacion',
            'allergies' => 'Ninguna.',
            'conditions' => 'Ninguna.',
            'idMedic' => '205544438',
        ]);

        factory(Patient::class)->create([
            'name' => 'Adrian Everardo Segura Garcia',
            'email' => 'Segura@hotmail.com',
            'phone' => '33112211',
            'birthdate' =>  '1997-03-21',
            'height' => "1.90",
            'weight' => "60",
            'description' => 'Gastroenteritis',
            'allergies' => 'Ninguna.',
            'conditions' => 'Ninguna.',
            'idMedic' => '205544438',
        ]);                

        factory(Patient::class, 10)->create();
    }
}
