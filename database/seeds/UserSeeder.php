<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_secretary = Role::where('name', 'secretary')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_medic = Role::where('name', 'medic')->first();

        $user = new User();
        $user->name = 'James Wilson';
        $user->email = 'jameWilson@hotmail.com';
        $user->password = bcrypt('secret');
        $user->idProfile = 87654321;
        $user->save();
        $user->roles()->attach($role_medic);

        $user = new User();
        $user->name = 'Juan Ayala Lopez';
        $user->email = 'userAdmin@example.com';
        $user->password = bcrypt('secret');
        $user->idProfile = 87654321;
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Jessica Adams';
        $user->email = 'JessicaAdamns@hotmail.com';
        $user->password = bcrypt('secret');
        $user->idProfile = 12345678;
        $user->save();
        $user->roles()->attach($role_secretary);

        $user = new User();
        $user->name = 'Gregory House';
        $user->email = 'GregoryHouse@hotmail.com';
        $user->password = bcrypt('12345');
        $user->idProfile = 205544438;
        $user->save();
        $user->roles()->attach($role_medic);

    }
}
