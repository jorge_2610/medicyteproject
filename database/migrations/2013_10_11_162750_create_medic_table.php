<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medics', function (Blueprint $table) {
            $table->string("cedule")->unique();
            $table->string("name");
            $table->bigInteger("phone");
            $table->integer("consultinRoom");
            $table->string("days");
            $table->time("ArrivalTime");
            $table->time("EndingTime");
            $table->string("specialty");
            $table->primary('cedule');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medics');
    }
}
