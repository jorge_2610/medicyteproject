<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->date("birthdate");
            $table->bigInteger('phone')->nullable();
            $table->string('description')->nullable();
            $table->float("height")->nullable();
            $table->float("weight")->nullable();
            $table->string('allergies')->nullable();
            $table->string('conditions')->nullable();
            $table->string('idMedic');
            $table->foreign('idMedic')->references('cedule')->on('Medics')->onUpdate('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
