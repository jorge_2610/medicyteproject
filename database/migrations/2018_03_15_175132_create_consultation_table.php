<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->time('hour');
            $table->double('costo');
            $table->string('idMedic');
            $table->string('title');
            $table->string('description');
            $table->boolean('active')->default(true);
            $table->integer('idPatient')->unsigned();
            $table->foreign('idMedic')->references('cedule')->on('Medics')->onUpdate('cascade');
            $table->foreign('idPatient')->references('id')->on('patients')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultations');
    }
}
