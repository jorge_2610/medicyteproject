<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Patient::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => 12345678,
        'description' => $faker->sentence(3,false),
        'birthdate' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'height' => $faker->numberBetween($min = 1.10, $max = 2.50),
        'weight' => $faker->numberBetween($min = 50, $max = 150),
        'idMedic' => '12345678',
    ];
});