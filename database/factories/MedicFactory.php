<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Medic::class, function (Faker $faker) {
	return [
		'cedule' => $faker->numberBetween(10000000,99999999),
		'name' => $faker->name,
		'phone' => $faker->numberBetween(330000000,339999999),
		'consultinRoom' => $faker->randomDigit,
		'days' => 'LMIJV',
		'EndingTime' => '15:00:00',
		'ArrivalTime' => '07:00:00',
		'specialty' => $faker->catchPhrase,
	];
});
