<?php

use Faker\Generator as Faker;

$factory->define(App\Prescription::class, function (Faker $faker) {
	return [
		'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
		'idConsultation' => $faker->numberBetween(1, 10)
	];
});
