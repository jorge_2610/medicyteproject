<?php

use Faker\Generator as Faker;

$factory->define(App\Consultation::class, function (Faker $faker) {
    return [
		'idPatient' => $faker->numberBetween($min = 1, $max = 50),
		'idMedic' => 12345678,
		'date' =>  $faker->date($format = 'Y-m-d', $max = 'now'),
		'costo' => $faker->numberBetween($min = 100, $max = 1000),
		'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
	];
});
