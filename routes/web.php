<?php
Auth::routes();

Route::get('/validar','UserController@index')->name('user.index');

Route::get('/patients', 'PatientController@index')->name('patients.index');

Route::get('/patients/{id}', 'MedicoController@showPatient')->where('id', '[0-9]+')->name('patients.show');

Route::get('/settings', 'MedicoController@settings')->name('settings');

Route::get('/editPatient/{id}', 'MedicoController@editPatient');

Route::get('/medicalRecord/{id}', 'MedicoController@medicalRecord');

Route::get('/results/{id}', 'MedicoController@medicalResults');

Route::post('/updatePatient', 'MedicoController@updatePatient');

Route::get('/schedule/{cedule}', 'MedicoController@schedule')->name('medic.schedule');

Route::post('mod/{id}', 'MedicoController@edit');

Route::post('update/{id}', 'UserController@edit');

Route::post('/closediagnostic', 'MedicoController@updateDiagnostic');

Route::post('updt/{id}', 'MedicoController@editTime');

Route::get('/diagnostic/{id}', 'MedicoController@editDiagnostic');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index');

Route::get('/index', 'SecretaryController@index');

Route::get('/scheduleSecretary', 'SecretaryController@schedule');

Route::get('/consultation/edit/{id}', 'SecretaryController@editDate');

Route::post('/editConsultation', 'SecretaryController@updateDate');

Route::get('/scheduleSecretaryManage', 'SecretaryController@scheduleManage');

Route::post('/scheduleSecretaryManage/delete', 'SecretaryController@deleteDate');

Route::get('/settingsSecretary', 'SecretaryController@settings')->name('secretary.settingsSecretary');

Route::post('editSecretary/{email}', 'SecretaryController@edit');

Route::get('/secretary/patients', 'SecretaryController@patients')->name('secretary.patients');

Route::get('/consultation', 'SecretaryController@consultationForm')->name('consultation.consultation');

Route::post('/addConsultation', 'SecretaryController@addConsultation');

Route::post('updateSecretary/{email}', 'UserController@editSecretary');

Route::get('addPatient','SecretaryController@patientFormView');

Route::post('createPatient', 'SecretaryController@createPatient');

Route::get('testAdmin', 'AdminController@index');

Route::get('backup/create', 'AdminController@create');

Route::get('backup/download/{file_name}', 'AdminController@download');

Route::get('backup/restore/{file_name}', 'AdminController@restore');

Route::get('backup/delete/{file_name}', 'AdminController@delete');

Route::get('create', 'AdminController@respaldoPhp');

Route::get('usersAdmin', 'AdminController@users');

Route::get('settingsAdmin', 'AdminController@settings');

Route::get('addUserView', 'AdminController@addUserView');

Route::post('updateAdmin/{id}', 'AdminController@updateAdmin');

Route::post('addUser', 'AdminController@addUser');

Route::get('editUserView/{id}', 'AdminController@editUserView')->where('id', '[0-9]+')->name('admin.editUserView');

Route::post('editUser', 'AdminController@editUser');

Route::get('deleteUser/{id}', 'AdminController@deleteUser')->where('id', '[0-9]+')->name('admin.deleteUser');